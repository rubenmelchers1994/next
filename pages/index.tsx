import { GetStaticProps, GetStaticPaths } from "next";
import Head from "next/head";
import Footer from "../components/Layouts/footer";

import LandingTemplate from '../components/Templates/LandingTemplate';
import { getSiteMeta, getHomePageData } from "../lib/queries";
import styles from "../styles/Main.module.scss";
import styled from 'styled-components';

export const getStaticProps: GetStaticProps = async () => {
  // const content = await getSiteMeta();
  const content = await getHomePageData();

  return {
    props: content,
    revalidate: 1,
  };
};

export default function Home({ content }) {
  // console.log("content", content);

  return (
    <div>
      <Head>
        <title>{content.page.seo.title}</title>
      </Head>

      <main className={styles.main}>
        <div className={styles.main_bg}></div>
        <LandingTemplate {...content.page.home} />
      </main>

      <Footer {...content.menu} />
    </div>
  );
}
