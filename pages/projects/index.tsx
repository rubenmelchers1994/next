import { GetStaticProps, GetStaticPaths } from "next";
import Head from "next/head";
import Footer from "../../components/Layouts/footer";
import projects from "../../styles/Projects.module.scss";
import OverviewProject from '../../components/molecules/OverviewProject';
import Gear from '../../components/molecules/Gear';
import { getProjectsPageData } from '../../lib/queries';
import Contact from '../../components/Templates/LandingTemplate/contact';
import React from "react";
import { useRef, useState, useLayoutEffect } from 'react'

export const getStaticProps: GetStaticProps = async () => {
    const content = await getProjectsPageData();
    return {
        props: content,
        revalidate: 1,
    };
};

const useWindowSize = () => {
    const [size, setSize] = useState([0, 0]);
    useLayoutEffect(() => {
        function updateSize() {
            setSize([window.innerWidth, window.innerHeight]);
        }
        window.addEventListener('resize', updateSize);
        updateSize();
        return () => window.removeEventListener('resize', updateSize);
    }, []);
    return size;
}

export default function Projects({ content }) {
    const offset = 0;
    const containerRef = useRef<HTMLDivElement>(null);

    const [width, height] = useWindowSize();
    let gutterWidth = 0;
    const el = containerRef.current || null;
    gutterWidth = el?.offsetLeft;

    // if( width <= 768) {
    // }
    const isSmall = width <= 768 ? true : false;
    // console.log(width);

    // const isMobil

    content.projects.nodes.sort((a,b) => {
        const aOrder = a.menuOrder ? a.menuOrder : 9999;
        const bOrder = b.menuOrder ? b.menuOrder : 9999;
        if(aOrder < bOrder) {
            return -1;
        }
        if(aOrder > bOrder) {
            return 1;
        }
        return 0;
    })
    return (
        <div>
            <main className={projects.projects}>
                <div className={projects.gearsWrapper}>
                    <Gear {...{x:72, y:-5, mobileX:80, mobileY:-5, size:'large', isSmall: isSmall}} />

                    <Gear {...{x:-15, y:10, mobileX:55, mobileY:5, size:'large', isSmall: isSmall, checkWidth: useWindowSize()}} />
                    <Gear {...{x:-8, y:18.5, size:'medium', reversed:true}}/>
                    <Gear {...{x:-8, y:22, size:'small', isBlurred:true}}/>

                    <Gear {...{x:95, y:22, size:'medium'}}/>
                    <Gear {...{x:88, y:28, size:'small', reversed:true}} />

                    <Gear {...{x:-12, y:43, size:'large'}} />
                    <Gear {...{x:-12, y:51.5, size:'medium', reversed:true}}/>
                    <Gear {...{x:-7, y:53.5, size:'small', isBlurred:true}}/>

                    <Gear {...{x:95, y:60, size:'large'}} />
                    <Gear {...{x:90, y:68, size:'medium', reversed:true}}/>
                    <Gear {...{x:88, y:65, size:'small', isBlurred:true}}/>

                </div>
                <div className="container" ref={containerRef}></div>
                <div className={projects.projects_header} style={{backgroundImage: `url(${content.page.projectspage.headerImage.sourceUrl})`, right: gutterWidth ? `${gutterWidth}px` : ''}}>
                    <div className="row">
                        <div className="col-12">
                            <div className={projects.projects_pageTitleWrapper}>
                                <a className={projects.projects_crumb} href="/">
                                    home
                                </a>
                                <div className={projects.projects_divider}>/</div>
                                <h1 className={projects.projects_title}>
                                    Work
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
                {content.page && content.page.projectspage && content.page.projectspage.introText && 
                    <div className={projects.projects_intro}>
                        <div className="container">
                            <div className="row">
                                <div className="col-10 col-md-7">
                                    <div className={projects.projects_introText} dangerouslySetInnerHTML={{__html: content.page.projectspage.introText}} />
                                </div>
                            </div>
                        </div>
                    </div>
                }


                <div className={projects.projects_overview}>
                    <div className="container">
                        <div className="row">
                            {content.projects && content.projects.nodes.length > 0 && content.projects.nodes.map((project: { title: {}; project: { featuredImage: { altText: any; sourceUrl: any; }; }; }, index: number) => {
                                return (
                                    <div key={index} className="col-12">
                                        <OverviewProject project={project} index={index} />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
                <Contact {...content.page.projectspage} bubble={true} />
            </main>
            <Footer {...content.menu} />
        </div>
    )
}