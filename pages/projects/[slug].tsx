import { GetStaticProps, GetStaticPaths } from "next";
import Head from "next/head";
import { getSiteMeta, getHomePageData, getProjectData, getProjectsList, getProjectsPageData } from "../../lib/queries";
import styles from "../../styles/Project.module.scss";
import general from "../../styles/Main.module.scss";
import { useRouter } from 'next/router'
import { useLayoutEffect, useRef, useState } from "react";
import Sticky from 'react-sticky-el';
import Contact from '../../components/Templates/LandingTemplate/contact';
import Related from '../../components/Templates/Related';
import Footer from "../../components/Layouts/footer";
import Image from 'next/image';
import { LazyLoadImage, LazyLoadComponent } from 'react-lazy-load-image-component';

export const getStaticPaths: GetStaticPaths = async () => {
  const content = await getProjectsPageData();
  // console.log('TEST', content);
  const paths = content.content.projects.nodes.map((post) => ({
      params: { id: post.id, slug: post.slug },
  }))
  return { paths, fallback: false }
}

const useWindowSize = () => {
  const [size, setSize] = useState([0, 0]);
  useLayoutEffect(() => {
  function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
  }
  window.addEventListener('resize', updateSize);
  updateSize();
      return () => window.removeEventListener('resize', updateSize);
  }, []);
  return size;
}

export async function getStaticProps({ params, preview = false, previewData }) {
  const data = await getProjectsList().then((res:any) => {
    const projects = res.content.projects.nodes;
    // console.log('res', res);
    const _id = projects.find(proj => proj.slug === params.slug).id;
    return _id;
  }).then(async id => {
    return await getProjectData(id);
  }).then(res => res.content);

  return {
    props: {
      preview,
      post: data && data.project ? data.project : null,
      menu: data && data.menu ? data.menu : null
    },
    revalidate: 1
  }
}


const Project = ({post}) => {
    // console.log("content", post);

    const router = useRouter()
    const { slug } = router.query

    const containerRef = useRef<HTMLDivElement>(null);

    const [width, height] = useWindowSize();
    let gutterWidth = 0;
    const el = containerRef.current || null;
    gutterWidth = el?.offsetLeft;

  return (
    <div>
      <Head>
        <title>{post.seo.title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.project}>
        <div className="container" ref={containerRef}></div>
          <div className={styles.project_header} style={{backgroundImage: `url(${post.project && post.project.headerImage && post.project.headerImage.sourceUrl ? post.project.headerImage.sourceUrl : ''})`, right: gutterWidth ? `${gutterWidth}px` : ''}}>
              <div className="row">
                  <div className="col-12">
                      <div className={styles.project_pageTitleWrapper}>
                          <a className={styles.project_crumb} href="/">
                              home
                          </a>
                          <div className={styles.project_divider}>/</div>
                          <a className={styles.project_crumb} href="/projects">
                              work
                          </a>
                          <div className={styles.project_divider}>/</div>
                          <h1 className={styles.project_title}>
                              {slug}
                          </h1>
                      </div>
                  </div>
              </div>
          </div>

          <div className={styles.project_body}>
            <div className="container">
              <div className="row">
                <div className="col-12 col-md-6 order-2 order-md-1">
                  <div className={styles.project_image}>
                    {post.project 
                    && post.project.pageImage
                    && post.project.pageImage.length > 0 
                    && post.project.pageImage.map((image, index: number) => {
                        return (
                            <div key={index} className={styles.project_pageImage}>
                                <LazyLoadImage
                                    effect="blur"
                                    alt={image.image.altText}
                                    src={image && image.image && image.image.sourceUrl ? image.image.sourceUrl : ''} 
                                />
                            </div>
                        )
                    })}
                  </div>
                </div>
                <div className="col-12 col-md-6 order-1 order-md-2">
                  <div id="sticky_wrapper" className={styles.project_description}>
                    <Sticky className={styles.project_stickyWrapper} boundaryElement="#sticky_wrapper">
                        <div className={styles.project_descriptionInner} dangerouslySetInnerHTML={{ __html: post.project.description }} />
                    </Sticky>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Contact {...post.project} />
          {post.project && post.project.projects && post.project.projects.length >= 0 &&
            <div>
              <Related title={post.project.relatedHeading} {...post.project} />
            </div>
          }
      </main>
      <Footer {...post.menu} />
    </div>
  )
}

export default Project;


// export default function Project({ content }) {
// console.log("content", content);

// return (
//     <div>
//     <Head>
//         <title>{content.page.seo.title}</title>
//         <link rel="icon" href="/favicon.ico" />
//     </Head>

//     <main className={styles.main}>
//         <LandingTemplate {...content.page.home} />
//     </main>

//     <Footer {...content.menu} />
//     </div>
// );
// }
  

// export default function handler(req, res) {
//     const {
//       query: { pid },
//     } = req
  
//     res.end(`Post: ${pid}`)
//   }