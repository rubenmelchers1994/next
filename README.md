This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) and flavoured with Typescript.

## Hit the ground running:
- Clone this repo into your own project
- Set up your MAIN and DEVELOP branch via BITBUCKET
- Go into VERCEL and set up that environment
- Go to WPENGINE and set up your new environment
- Install: 
  - advanced-custom-fields-pro
  - classic-editor
  - gatsby-toolkit
  - wp-graphiql
  - wp-graphql
  - wp-graphql-cors
  - wp-graphql-acf
  You'll likely you just grab the zips from a previous project. And upload them via the WP CMS interface
- Hook up WPENGINE to your local NEXT site:
  By now you can now reach your Wordpress Graphql interface via https://mycoolwpenginesite.com/graphql
  By now your VERCEL site would have finished setting up as well
  - Open your CLI and do: 
  - `vercel link` to link your local copy to the remote VERCEL instane
  - `vercel env add GRAPHQL_API_URL` and follow the prompts to add your GQL interface to the project.
  - `vercel env pull` to finally create the ENV file which is synced up to the VERCEL environment
    This will make /lib/client.ts work correctly
- An .env file should have been generated. Map the GraphQL API in the file like so: `GRAPHQL_API_URL=www.example.com/graphql`

## Accessing a WP Engine site that is LOCKED
Your GQL interface (e.g. https://mycoolwpenginesite.com/graphql) will be asking for a username and password if you try to hit that via a browser. 

You could either open up the WP engine to the public, or pass in a basic authorization token via the header of the gql client. 

Hit your GQL interface, enter your credentials in the pop up, and open up the network tab. Open the HEADERS tab of your graphql request. Scroll down and under REQUEST HEADERS you can find the AUTHORIZATION value. It will look something like
`authorization: Basic AGVtsdoxZjh2OSgzMhhhZTY=`
You might need to refresh for it to show up in the network tab.

Take the value `Basic AGVtsdoxZjh2OSgzMhhhZTY=` and add it into a new ENV variable via VERCEL's CLI:
`vercel env add CMS_AUTH_TOKEN`
Add this to all your VERCEL instances, except PRODUCTION.

## Deploy on Vercel
If you set things up correctly in Vercel. Your branches will automagically produce a test link on VERCEL
Deploying to DEVELOP or MAIN branches will update the staging and production URLs respectively. 

You can change this pretty easily within the Settings in Vercel.

## Git Flow
We maintain a strict commit strategy to minimise conflicts and errors when working in a team. 
[Read more about it here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) if you're unsure what to do.

Basically; you keep the DEVELOP branch as the single source of truth.
branch off from the DEVELOP branch, using feature/feature_name or bugfix/bugfix_name. 
When you're done, merge the develop branch into your branch, take care of any conflicts if there are any. Then merge your branch into develop. And delete your branch after.

If these changes need to go into a live site, then merge DEVELOP into the MAIN branch.

An exception is when you have a HOTFIX (e.g. you made a typo, or you missed something extremely minor).
You can apply this directly into the MAIN branch, but as long as you then also MERGE the MAIN branch into DEVELOP so that stays up to date.

## Coming Soon
It can be helpful to start your project by creating a branch to hold a coming soon page. If you push this branch into Bitbucket, then this will become available in VERCEL. And you can then assign this branch as your PRODUCTION branch. 

This allows you to set up your domain settings from the get-go and you just have to change the PRODUCTION back to the MAIN branch once you're ready to go live. 

This applies only for new sites. For other projects your mileage may vary.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Next.js on ENV Variables](https://vercel.com/blog/environment-variables-ui) - Manage ENV vars via Vercel 
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details on deployments

 

