import { gql } from "graphql-request";
import client from "./client";

// Basic implementation of a GQL query to a WP ENGINE site with WPGQL enabled
export const getSiteMeta = async () => {
  const GET_SITE_META = gql`
    {
      generalSettings {
        title
        url
        description
      }
    }
  `;
  const content = await client
    .request(GET_SITE_META)
    .then((data) => data.generalSettings)
    .catch((reason) => console.error(reason));
  return { content };
};

export const getHomePageData = async () => {
  const GET_HOMEPAGE_DATA = gql`
  {
    menu(id: "dGVybToy") {
      name
      menuItems {
        nodes {
          path
          title
        }
      }
    }
    page(id: "cG9zdDoyMA==") {
      title
      home {
        aboutContent
        aboutTitle
        contactContent
        contactTitle
        experienceIntro
        experienceTitle
        fieldGroupName
        location
        projects {
          ... on Project {
            id
            title
            slug
            project {
              link
              featuredText
              featuredImage {
                link
                altText
                sourceUrl
                title(format: RAW)
                srcSet
              }
              featuredImageHomepage {
                link
                altText
                sourceUrl
                title(format: RAW)
                srcSet
              }
              headerImage {
                link
                altText
                sourceUrl
                title(format: RAW)
                srcSet
              }
            }
          }
        }
        subtitle
        title
        totalYears
        workTitle
        skills {
          name
          dates {
            startdate
            enddate
          }
        }
        introImage {
          altText
          srcSet
          sourceUrl
          uri
        }
        contactImage {
          altText
          srcSet
          sourceUrl
          uri
          title
        }
        aboutImage {
          altText
          title
          srcSet
          sourceUrl
        }
      }
      seo {
        canonical
        metaDesc
        metaRobotsNoindex
        title
        opengraphUrl
        opengraphType
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphDescription
        opengraphAuthor
        metaRobotsNofollow
        cornerstone
        focuskw
        breadcrumbs {
          text
          url
        }
        metaKeywords
        twitterDescription
        opengraphImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
        twitterTitle
        twitterImage {
          sourceUrl
        }
      }
    }
  }
  `;
  const content = await client
    .request(GET_HOMEPAGE_DATA)
    .then((data) => data)
    .catch((reason) => console.error(reason));
  return { content };
}

export const getProjectsPageData = async () => {
  const GET_PROJECTS_PAGE_DATA = gql`
    {
      menu(id: "dGVybToy") {
        name
        menuItems {
          nodes {
            path
            title
          }
        }
      }
      page(id: "cG9zdDoyOA==") {
        id
        title
        projectspage {
          contactContent
          contactTitle
          introText
          headerImage {
            link
            altText
            sourceUrl
            title(format: RAW)
            srcSet
          }
          contactImage {
            link
            altText
            sourceUrl
            title(format: RAW)
            srcSet
          }
        }
        seo {
          canonical
          metaDesc
          metaRobotsNoindex
          title
          opengraphUrl
          opengraphType
          opengraphTitle
          opengraphSiteName
          opengraphPublisher
          opengraphPublishedTime
          opengraphModifiedTime
          opengraphDescription
          opengraphAuthor
          metaRobotsNofollow
          cornerstone
          focuskw
          breadcrumbs {
            text
            url
          }
        }
      }
      projects {
        nodes {
          link
          slug
          id
          title
          menuOrder
          project {
            overviewText
            featuredImage {
              link
              altText
              sourceUrl
              title(format: RAW)
            }
          }
        }
      }
    }
  `
  const content = await client
    .request(GET_PROJECTS_PAGE_DATA)
    .then((data) => {return data})
    .catch((reason) => console.error(reason));
  return { content };
}

export const getFooterPages = async () => {
  const GET_FOOTER_LINKS = gql`
    {
      page(id: "cG9zdDoyOA==") {
        id
        slug
        projectspage {
          contactContent
          contactTitle
        }
      }
    }
  `;
  const content = await client
    .request(GET_FOOTER_LINKS)
    .then((data) => data)
    .catch((reason) => console.error(reason));
  return { content };
}

export const getProjectsList = async () => {
  const GET_PROJECTS_LIST = gql`
    {
      projects {
        nodes {
          id
          slug
          title
          link
          project {
            client
            clientSite
            contactContent
            contactImage {
              sourceUrl
              srcSet
            }
            featuredImageHomepage {
              sourceUrl
              srcSet
              altText
            }
            featuredImage {
              sourceUrl
              srcSet
              altText
            }
            headerImage {
              sourceUrl
              srcSet
              altText
            }
            contactTitle
            description
            featuredText
            overviewText
            fieldGroupName
            link
            startdate
          }
        }
      }
      menu(id: "dGVybToy") {
        name
        menuItems {
          nodes {
            path
            title
          }
        }
      }
    }
  `
  const content = await client
    .request(GET_PROJECTS_LIST)
    .then((data) => data)
    .catch((reason) => console.error(reason));
  return { content };
}

export const getProjectData = async (id) => {
  const GET_PROJECT_DATA = gql`
  {
    project(id: "${id}") {
      id
      title
      slug
      project {
        client
        clientSite
        contactContent
        contactTitle
        contactImage {
          sourceUrl
          srcSet
          altText
          title
        }
        pageImage {
          image {
            sourceUrl
            srcSet
            altText
          }
        }
        description
        link
        startdate
        tech
        fieldGroupName
        featuredImage {
          altText
          srcSet
          sourceUrl
          title
        }
        headerImage {
          altText
          sourceUrl
          slug
          srcSet
          title
        }
        relatedHeading
        projects {
          ... on Project {
            id
            slug
            title
            project {
              featuredImageHomepage {
                sourceUrl
                srcSet
              }
            }
          }
        }
      }
      seo {
        canonical
        metaDesc
        metaRobotsNoindex
        title
        opengraphUrl
        opengraphType
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphDescription
        opengraphAuthor
        metaRobotsNofollow
        cornerstone
        focuskw
        breadcrumbs {
          text
          url
        }
      }
    }
    menu(id: "dGVybToy") {
      name
      menuItems {
        nodes {
          path
          title
        }
      }
    }
  }
  `;

  const content = await client
    .request(GET_PROJECT_DATA)
    .then((data) => data)
    .catch((reason) => console.error(reason));
  return { content };
}