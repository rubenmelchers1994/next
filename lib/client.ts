/* API: https://github.com/prisma-labs/graphql-request */
import { GraphQLClient } from "graphql-request";

// Did you VERCEL ENV PULL and everything? Check README for more troubleshooting.
if (!process.env.GRAPHQL_API_URL) {
  throw new Error("GQL URL is not set. Check your ENV setup");
}
if (!!process.env.CMS_AUTH_TOKEN) {
  console.log('CMS token set')
}

const client = new GraphQLClient(process.env.GRAPHQL_API_URL, {
  headers: {
    // pass in a basic auth in case WPENGINE instance is locked. 
    // This is a basic B64 encode of USERNAME:PASSWORD
    authorization: process.env.CMS_AUTH_TOKEN || "",
  },
});

export default client;
