import React from "react";
// import { ThemeProvider } from "styled-components";

// import Meta from "../components/Meta";
// import GlobalStyle from "../styles/GlobalStyle";

const Page = ({ children }) => {
  return (
    <>
      {/* <GlobalStyle /> */}
      {/* <Meta /> */}
      {children}
    </>
  );
};

export default Page;
