import React, { useLayoutEffect, useState } from 'react'
import  { ScrollRotate } from 'react-scroll-rotate';
import styles from "../../styles/Projects.module.scss";
import debounce from 'lodash.debounce';

class Gear extends React.Component<{}, { useX:number, useY:number,timeout: any, x:number, mobileX:number, y:number, mobileY: number, size:string, isBlurred:boolean, reversed:boolean, isSmall:boolean, checkWidth:any }> {
    constructor(props) {
        super(props);
        this.state = {
            isSmall: props.isSmall,
            x: props.x,
            y: props.y,
            mobileX: props.mobileX,
            mobileY: props.mobileY,
            size: props.size,
            isBlurred: props.isBlurred,
            reversed: props.reversed,
            checkWidth: '',
            timeout: '',
            useX: 0,
            useY: 0
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange() {
        const self = this;
        // console.log('TEST', self);
        this.setState({
            ...this.state,
            useX: this.state.isSmall && this.state.mobileX ? this.state.mobileX : this.state.x,
            useY: this.state.isSmall && this.state.mobileY ? this.state.mobileY : this.state.y,
        });
    }

    render() {
        let size = '';
        let timeout = '';
        if(this.state.size === 'small') size = '300px';
        if(this.state.size === 'medium') size = '400px';
        if(this.state.size === 'large') size = '500px';
        

        // let _x = this.state.x;
        // let _y = this.state.y;

        // if(this.state.isSmall) {
        //     _x = this.state.mobileX;
        //     _y = this.state.mobileY;
        // }
        // console.log(this.props);

        return (
            <div className={`${styles.gearWrapper} ${this.state.isBlurred ? styles.gearWrapper__blurred : ''}`} style={{ top: `${this.state.useY}%`, left: `${this.state.useX}vw`, width: size, height: size }}>
                <ScrollRotate from={0} to={this.state.reversed ? -360 : 360} loops={1} method={"perc"}>
                    <div className={styles.gear}></div>
                </ScrollRotate>
            </div>
        )
    }
    componentDidUpdate(e) {
        const debouncedUpdate = debounce(() => {
            this.handleChange();
        }, 500)
        debouncedUpdate();

    }
}

export default Gear;