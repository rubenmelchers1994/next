import React from 'react'
import styles from '../../styles/Work.module.scss';

class ProjectCard extends React.Component<{}, { title: string, project: any, slug: string }> {
    constructor(props) {
        super(props);
        this.state = {
            title: props.title,
            project: props.project,
            slug: props.slug
        };
    }
    
    render() {
        return (
            <div className={styles.work__card}>
                <a href={`projects/${this.state.slug}`}>
                    <div className={styles.work__projectTitle}>
                        { this.state.title }
                    </div>
                    {this.state.project.featuredImage &&
                        <img src={this.state.project.featuredImage.link} srcSet={this.state.project.featuredImage.srcSet} alt={this.state.project.featuredImage.altText}/>
                    }
                    {!this.state.project.featuredImage && this.state.project.headerImage &&
                        <img src={this.state.project.headerImage.link} srcSet={this.state.project.headerImage.srcSet} alt={this.state.project.headerImage.altText}/>
                    }
                    {!this.state.project.featuredImage && !this.state.project.headerImage &&
                        <div className="no-image"></div>
                    }
                </a>
            </div>
            )
        }
}
    
export default ProjectCard
    