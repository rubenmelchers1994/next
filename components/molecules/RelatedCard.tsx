import React from 'react'
import styles from '../../styles/Related.module.scss';
import { LazyLoadImage, LazyLoadComponent } from 'react-lazy-load-image-component';
import Link from 'next/link';
import { useRouter } from 'next/router';

class RelatedCard extends React.Component<{}, { title: string, image: object, slug: string }> {
    constructor(props) {
        super(props);
        this.state = {
            title: props.title,
            image: props.image,
            slug: props.slug
        };
    }
    
    render() {
        return (
            <div className={styles.related}>
                <div className={styles.related_link}>
                    {/* <Link href={`/projects/${this.state.slug}`} aria-label={this.state.title} >
                        <LazyLoadImage
                            effect="blur"
                            alt={this.state.title}
                            src={this.state.image['sourceUrl']} 
                            className={styles.related_image}
                        />
                    </Link> */}
                    <Link href={`/projects/${this.state.slug}`}>
                        <a aria-label={this.state.title} className={styles.related_link}>
                            <LazyLoadImage
                                effect="blur"
                                alt={this.state.title}
                                src={this.state.image['sourceUrl']} 
                                className={styles.related_image}
                            />
                        </a>
                    </Link>
                </div>
            </div>
            )
        }
}
    
export default RelatedCard
    