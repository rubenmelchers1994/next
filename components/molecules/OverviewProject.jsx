import React from 'react';
import projects from '../../styles/Projects.module.scss';
import { useSpring, animated } from 'react-spring';
import { LazyLoadImage, LazyLoadComponent } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import Image from 'next/image';

const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2];
const trans1 = (x, y) => `translate3d(${x / 10}px,${y / 10}px,0)`;
const trans2 = (x, y) => `translate3d(${x / 3.5}px,${y / 3.5}px,0)`;
const trans3 = (x, y) => `translate3d(${x / 6 - 15}px,${y / 6 - 20}px,0)`;

const OverviewProject = (properties) => {
    const [props, set] = useSpring(() => ({ xy: [0, 0], config: { mass: 10, tension: 550, friction: 140 } }));
    return (
        <div onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}>
            {properties.index % 2 === 0 &&
                // Uneven rows
                <div className={`row ${projects.projects_row} ${!properties.project.project.overviewText ? projects.projects_row__moreSpace : ''}`} onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}>
                    <a href={`/projects/${properties.project.slug}`} aria-label={properties.project.title} className={projects.projects_projectLink}></a>
                    <div className="col-12">
                        <div className={`${projects.projects_titleWrapper} ${projects.projects_titleWrapper__right}`}>
                            <animated.div className={projects.projects_titleInner} style={{ transform: props && props.xy ? props.xy.interpolate(trans2) : '' }}>
                                <h2 className={projects.projects_projectTitle} >
                                    {properties.project.title}
                                </h2>
                            </animated.div>
                        </div>
                    </div>
                    <div className="col-9 offset-1">
                        <animated.div style={{ transform: props && props.xy ? props.xy.interpolate(trans1) : ''}}>
                            <div className={`${projects.projects_imageWrapper} img-wrapper`}>
                                {/* <LazyLoadImage
                                    effect="blur"
                                    alt={properties.project.project.featuredImage.altText}
                                    src={properties.project.project.featuredImage.sourceUrl} 
                                    className={projects.projects_image}
                                /> */}
                                <Image 
                                    alt={properties.project.project.featuredImage.altText}
                                    src={properties.project.project.featuredImage.sourceUrl} 
                                    className={projects.projects_image}
                                    layout={'fill'}
                                />
                            </div>
                        </animated.div>
                    </div>
                    {properties.project.project.overviewText && 
                        <div className={`col-12 ${projects.projects_blockRow}`}>
                            <div className={`${projects.projects_blockWrapper}`}>
                                <animated.div className={`${projects.projects_block} ${projects.projects_block__right}`} style={{ transform: props && props.xy ? props.xy.interpolate(trans3) : '' }} dangerouslySetInnerHTML={{__html: properties.project.project.overviewText}} />
                            </div>
                        </div>
                    }
                </div>
            }

            {properties.index % 2 === 1 && 
                // Even rows
                <div className={`row ${projects.projects_row} ${!properties.project.project.overviewText ? projects.projects_row__moreSpace : ''}`} onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}>
                    <a href={`/projects/${properties.project.slug}`} aria-label={properties.project.title} className={projects.projects_projectLink}></a>
                    <div className="col-12">
                        <div className={`${projects.projects_titleWrapper}`}>
                            <animated.div className={projects.projects_titleInner} style={{ transform: props && props.xy ? props.xy.interpolate(trans2) : '' }}>
                                <h2 className={projects.projects_projectTitle} >
                                    {properties.project.title}
                                </h2>
                            </animated.div>
                        </div>
                    </div>
                    <div className="col-9 offset-1">
                        <animated.div style={{ transform: props && props.xy ? props.xy.interpolate(trans1) : '' }}>
                            <div className={projects.projects_imageWrapper}>
                                {/* <LazyLoadImage
                                    alt={properties.project.project.featuredImage.altText}
                                    effect="blur"
                                    src={properties.project.project.featuredImage.sourceUrl} 
                                    className={projects.projects_image}
                                /> */}
                                <Image 
                                    alt={properties.project.project.featuredImage.altText}
                                    src={properties.project.project.featuredImage.sourceUrl} 
                                    className={projects.projects_image}
                                    layout={'fill'}
                                />
                            </div>
                        </animated.div>
                    </div>
                    {properties.project.project.overviewText && 
                        <div className={`col-12 ${projects.projects_blockRow}`}>
                            <div className={`${projects.projects_blockWrapper}`}>
                                <animated.div className={`${projects.projects_block}`} style={{ transform: props && props.xy ? props.xy.interpolate(trans3) : '' }} dangerouslySetInnerHTML={{__html: properties.project.project.overviewText}} />
                            </div>
                        </div>
                    }
                </div>
            }
        </div>
    )
}
    
export default OverviewProject
    