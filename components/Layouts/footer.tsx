import { GetStaticProps } from 'next';
import React from 'react'
import { getFooterPages, getHomePageData } from '../../lib/queries';
import styles from "../../styles/Footer.module.scss";

export default function Footer({content}) {
    return (
        <footer className={styles.footer}>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className={styles.footer__inner}>
                                <div className={styles.footer__copyright}>
                                    Copyright © Ruben Melchers {new Date().getFullYear()}
                                </div>
                                <div className={styles.footer__poweredWrapper}>
                                    <a href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app" target="_blank" rel="noopener noreferrer" className={styles.footer__powered}>
                                        Powered by <img src="/vercel.svg" alt="Vercel Logo" className={styles.footer__logo} />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
    )
}