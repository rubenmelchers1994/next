import React from 'react'
import styled from 'styled-components';
import mainStyle from '../../../styles/Main.module.scss';
import styles from '../../../styles/Work.module.scss';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
// import { Carousel } from 'react-responsive-carousel';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import ProjectCard from '../../molecules/ProjectCard';
import { LazyLoadImage, LazyLoadComponent } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

class Work extends React.Component<{}, { title: string, projects: Array<Object|any> }> {
    constructor(props) {
        super(props);
        this.state = {
            title: props.workTitle,
            projects: props.projects
        };
    }

    
    render() {
        const firstProject = this.state.projects && this.state.projects[0] ? this.state.projects[0] : null;
        const secondProject = this.state.projects && this.state.projects[1] ? this.state.projects[1] : null;
        return (
            <div className={styles.work}>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h2 className="sectionTitle sectionTitle--right">
                                {this.state.title}
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {firstProject !== null && 
                                <LazyLoadComponent>
                                    <div className={`${styles.work_section} ${styles.work_section__first}`}>
                                        <div className={`${styles.work_background} ${styles.work_background__first}`}></div>
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-12 col-md-5">
                                                    <div className={styles.work_contentWrapper} dangerouslySetInnerHTML={{__html: firstProject.project.featuredText}}>
                                                    </div>
                                                    <div className={styles.work__contentLinkWrapper}>
                                                        <a href={`projects/${firstProject.slug}`} className={styles.work__contentLink} >Go to project</a>
                                                    </div>
                                                </div>
                                                <div className="col-11 col-md-5 offset-1">
                                                    <div className={styles.work_imageWrapper}>
                                                        {firstProject.project.featuredImage && 
                                                            <LazyLoadImage
                                                                alt={firstProject.project.featuredImage.altText}
                                                                effect="blur"
                                                                src={firstProject.project.featuredImage.sourceUrl} 
                                                                className={styles.work_image}
                                                            />
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </LazyLoadComponent>
                            }
                            {secondProject !== null && 
                                <LazyLoadComponent>
                                    <div className={`${styles.work_section} ${styles.work_section__second}`}>
                                        <div className={`${styles.work_background} ${styles.work_background__second}`}></div>
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-11 col-md-5 offset-1">
                                                    <div className={styles.work_imageWrapper}>
                                                        {secondProject.project.featuredImageHomepage && 
                                                            <LazyLoadImage
                                                                alt={secondProject.project.featuredImageHomepage.altText}
                                                                effect="blur"
                                                                src={secondProject.project.featuredImageHomepage.sourceUrl} 
                                                                className={styles.work_image}
                                                            />
                                                        }
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-5 offset-1">
                                                    <div className={`${styles.work_contentWrapper} ${styles.work_contentWrapper__bottom} ${styles.work_contentWrapper__lessHeight}`} dangerouslySetInnerHTML={{__html: secondProject.project.featuredText}}>
                                                    </div>
                                                    <div className={styles.work__contentLinkWrapper}>
                                                        <a href={`projects/${secondProject.slug}`} className={styles.work__contentLink} >Go to project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </LazyLoadComponent>
                            }
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className={styles.work_buttonWrapper}>
                                <a href="/projects" className={styles.work_button}>
                                    <span className={styles.work_buttonText}>
                                        Explore works
                                    </span>
                                    <img src="/arrow-right.svg" alt="" className={styles.work_buttonIcon} />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <div className={styles.work_projectsWrapper}>
                    <div className={styles.work_projectsInnerWrapper}>
                        <CarouselProvider
                            naturalSlideWidth={20}
                            naturalSlideHeight={20}
                            totalSlides={6}
                            visibleSlides={4}
                            currentSlide={0}
                            isIntrinsicHeight={true}
                        >
                            <Slider>
                                {this.state.projects.map((project, index) => {
                                    return (
                                        <Slide index={index}>
                                            <ProjectCard {...project}></ProjectCard>
                                        </Slide>
                                    )
                                })}
                            </Slider>
                            <ButtonBack>Back</ButtonBack>
                            <ButtonNext>Next</ButtonNext>
                        </CarouselProvider>
                    </div>
                </div> */}
            </div>
        );
    }
}

export default Work
