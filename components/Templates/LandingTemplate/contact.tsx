import React from 'react'
import styled from 'styled-components';
import mainStyle from '../../../styles/Main.module.scss';
import styles from '../../../styles/Contact.module.scss';
import Tilt from 'react-tilt';
import axios from 'axios';
import Contact from '../contact';
import { setTimeout } from 'timers';

class Work extends React.Component<{}, { title: string, content: string, image: string, name : string, mail : string, message : string, submittable : boolean, submitted: boolean, toast : string, bubble:boolean, self:any }> {
    constructor(props) {
        super(props);
        this.state = {
            title: props.contactTitle,
            content: props.contactContent,
            image: props.contactImage ? props.contactImage['sourceUrl'] : '/ruben_melchers_shot_between_monitors.jpg',
            name: '',
            mail: '',
            message: '',
            submittable: false,
            submitted: false,
            toast: 'Succesfully sent message!',
            bubble: props.bubble,
            self: this
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setSubmit = this.setSubmit.bind(this);
    }

    
    handleChange(event) {
        const value = event.target.value;
        const self = this;
        this.setState({
            ...this.state,
            [event.target.id]: value
        });
        setTimeout(() => {
            if(this.state.name !== '' && this.state.mail !== '' && this.state.message !== '') {
                self.setState({submittable: true});
            } else {
                self.setState({submittable: false});
            }
        }, 1);
    }
    
    setSubmit() {
        
    }
    
    handleSubmit(event) {
        event.preventDefault();

        const formUrl = 'https://wp.rubenmelchers.nl/wp-json/contact-form-7/v1/contact-forms/71/feedback';
        
        const formData = new FormData()
        formData.set('your-name', this.state.name)
        formData.set('your-email', this.state.mail)
        formData.set('your-message', this.state.message)
        const self = this;
        axios({
            method: 'post',
            url: formUrl,
            data: formData,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (response) {
                //handle success
                self.setState({
                    name: '',
                    mail: '',
                    message: '',
                    submitted: true
                })
                setTimeout(() => {
                    self.setState({submitted: false})
                }, 5000);
            })
            .catch(function (response) {
                //handle error
                console.log(response);
            });
    }


    render() {
        return (
            <div className={`${styles.contact} ${this.state.bubble ? styles.contact__bubble : ''}`}>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h2 className="sectionTitle">
                                {this.state.title}
                            </h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-7 order-2 order-md-1">
                            <div className={styles.contact_content}>
                                Want to find out more? Do you want to chat about some potential projects? Do you want to check out my resumé? Fill out the form below or send an email to info@rubenmelchers.nl and let’s talk!
                            </div>
                            <div>
                                <form onSubmit={this.handleSubmit} className={styles.contact_form}>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-6">
                                                <label htmlFor="your-name" className={styles.contact_label}>
                                                    <input type="text" name="your-name" placeholder="Your name" id="name" value={this.state.name} onChange={this.handleChange} className={styles.contact_formInput}/>
                                                </label>
                                            </div>
                                            <div className="col-6">
                                                <label htmlFor="your-email" className={styles.contact_label}>
                                                    <input type="email" name="your-email" placeholder="Your email address" id="mail" value={this.state.mail} onChange={this.handleChange} className={styles.contact_formInput}/>
                                                </label>
                                            </div>
                                            <div className="col-12">
                                                <label htmlFor="your-message" className={styles.contact_label}>
                                                    <input type="textarea" name="your-message" placeholder="A nice message that will appear in my inbox" id="message" value={this.state.message} onChange={this.handleChange} className={styles.contact_formTextarea}/>
                                                </label>
                                            </div>
                                            <div className="col-12">
                                                {this.state.submittable === false && 
                                                    <input type="submit" value="Send" disabled className={styles.contact_button}/>
                                                }
                                                {this.state.submittable === true && 
                                                    <input type="submit" value="Send" className={styles.contact_button}/>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-12 col-md-5">
                            <div className={styles.contact_bgWrapper}>
                                <div className={styles.contact_imageBg}></div>
                                <div className={styles.contact_back}>
                                    <Tilt className="Tilt" options={{ max : 25, scale: 1.05 }} style={{ height: '100%', width: '100%' }} >
                                        <div className={styles.contact_imageBack}></div>
                                        <div
                                            className={styles.contact_image}
                                        >
                                                <img src={this.state.image} alt="" className={styles.about__imageInner} />
                                        </div>
                                    </Tilt>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`${styles.contact_toast} ${this.state.submitted !== false ? styles.contact_toast__show : ''}`}>{this.state.toast}</div>
            </div>
        );
    }
}

export default Work
