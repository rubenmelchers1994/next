import React from 'react'
import Tilt from 'react-tilt';
import styles from '../../../styles/About.module.scss';
import { LazyLoadImage, LazyLoadComponent } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

class About extends React.Component<{}, { content: string, title: string, image: object }> {
    constructor(props) {
        super(props);
        this.state = {
            title: props.aboutTitle,
            content: props.aboutContent,
            image: props.aboutImage
        };
    }

    render() {
        return (
            <div className={styles.about}>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h2 className={`${styles.about__title} sectionTitle`}>
                                { this.state.title }
                            </h2>
                        </div>
                        <div className="col-12 col-md-4 order-2 order-md-1">
                            <div className={styles.about__bgWrapper}>
                                <div className={styles.about__imageBg}></div>
                                <div className={styles.about__back}>
                                    <Tilt className="Tilt" options={{ max : 25, scale: 1.05 }} style={{ height: '100%', width: '100%' }} >
                                        <div className={styles.about__imageBack}></div>
                                        <div
                                            className={styles.about__image}
                                        >
                                                <img src={this.state.image['sourceUrl']} alt="" className={styles.about__imageInner} />
                                        </div>
                                    </Tilt>
                                </div>
                            </div>
                        </div>
                        <div className="col-11 col-md-8">
                            <div className={styles.about__content} dangerouslySetInnerHTML={{ __html: this.state.content }} >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default About
