import React from 'react'
import Intro from './intro';
import About from './about';
import Work from './work';
import Contact from './contact';


class Home extends React.Component<{}, { content: object }> {
  constructor(props) {
      super(props);
      this.state = {
          content: props
      };
  }

  render() {
    return (
      <div>
        <Intro {...this.state.content} />
        <About {...this.state.content} />
        <Work {...this.state.content} />
        <Contact {...this.state.content} />
      </div>
    )
  }
}

export default Home
