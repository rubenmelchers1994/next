import React from 'react'
import Tilt from 'react-tilt';
import styled from 'styled-components';
import styles from '../../../styles/Main.module.scss';
import introStyling from '../../../styles/Intro.module.scss';
import { LazyLoadImage, LazyLoadComponent } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

class HomeIntro extends React.Component<{}, { title: string, subtitle: string, location: string, image: object }> {
    constructor(props) {
        super(props);
        this.state = {
            title: props.title,
            subtitle: props.subtitle,
            location: props.location,
            image: props.introImage
        };
    }

    render() {
        return (
            <div className={introStyling.intro}>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h1 className={introStyling.intro__title}>
                                {this.state.title}
                            </h1>
                        </div>
                        <div className="col-12 col-md-6">
                            <p className={introStyling.intro__sub}>
                                {this.state.subtitle}
                            </p>
                            <p className={introStyling.intro__sub}>{this.state.location}</p>
                        </div>
                        <div className="col-12 col-md-6">
                            <div className={introStyling.intro__bgWrapper}>
                                <div className={introStyling.intro__imageBg}></div>
                                <div className={introStyling.intro__back}>
                                    <Tilt className="Tilt" options={{ max : 25, scale: 1.05 }} style={{ height: '100%', width: '100%' }} >
                                        <div className={introStyling.intro__imageBack}></div>
                                        <div
                                            className={introStyling.intro__image}
                                        >
                                                {/* <img src="/ruben_melchers_close_up_shot_cutout.png" alt="" /> */}
                                            {/* <Tilt :reverse="true" :perspective="1500"> */}
                                            {/* </Tilt> */}
                                            <LazyLoadImage
                                                alt="Ruben Melchers, looking quirky"
                                                effect="blur"
                                                src="/ruben_melchers_close_up_shot_cutout.png" 
                                                className={styles.work_image}
                                            />
                                        </div>
                                    </Tilt>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// const HomeIntro = ({ component, props }) => {
//     console.log(props)
//     return (
//         <div>INTRO</div>
//     )
// }

export default HomeIntro
