import React from 'react'
import styles from "../../styles/Project.module.scss";
import RelatedCard from '../molecules/RelatedCard';

class Related extends React.Component<{}, { heading:string, projects:any }> {
    constructor(props) {
        super(props);
        this.state = {
            heading: props.title,
            projects: props.projects ? props.projects : []
        };
    }

    
    render() {
        console.log('state', this.state)
        return (
            <section className={styles.project_related}>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h2 className="sectionTitle sectionTitle--right">
                                { this.state.heading }
                            </h2>
                        </div>
                    </div>
                    <div className={`row ${styles.project_relatedRow}`}>
                        {this.state.projects.length > 0 && this.state.projects.map((project, index: number) => {
                            let projImage = {};
                            if(project.project.featuredImageHomepage) {
                                projImage = {
                                    sourceUrl: project.project.featuredImageHomepage.sourceUrl,
                                    srcSet: project.project.featuredImageHomepage.srcSet
                                }
                            }
                            if(!project.project.featuredImageHomepage && project.project.featuredImage) {
                                projImage = {
                                    sourceUrl: project.project.featuredImage.sourceUrl,
                                    srcSet: project.project.featuredImage.srcSet
                                }
                            }
                            return (
                                <div key={index} className="col-12 col-md-4">
                                    <RelatedCard 
                                        {...project}
                                        title={project.title}
                                        slug={project.slug}
                                        image={projImage}
                                    />
                                </div>
                            )
                        })}
                    </div>
                </div>
            </section>
        )
    }
}

export default Related;